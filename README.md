# first-app [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> First electrode app

## Installation

```sh
$ npm install --save first-app
```

## Usage

```js
var firstApp = require('first-app');

firstApp('Rainbow');
```
## License

Apache-2.0 © [bhargav018@icloud.com](bhargav.me)


[npm-image]: https://badge.fury.io/js/first-app.svg
[npm-url]: https://npmjs.org/package/first-app
[travis-image]: https://travis-ci.org/bhargav018/first-app.svg?branch=master
[travis-url]: https://travis-ci.org/bhargav018/first-app
[daviddm-image]: https://david-dm.org/bhargav018/first-app.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/bhargav018/first-app
